;; To run the example:
;; (load (compile-file "test.lisp"))
;; (let ((fac (build-fac-fun)))
;;   (run-fun fac 5))
;;
;; TADA! it outputs 120.

(require 'cl-llvm)
(use-package :llvm)

;; Make a factorial function!
(defun build-fac-fun ()
  (let* ((mod *jit-module*))
    ;; Build it
    (let* ((fac_args (list (LLVMInt32Type)))
           (fac (LLVMAddFunction mod "fac" (LLVMFunctionType (LLVMInt32Type)
                                                             fac_args nil)))
         ;; Create code-builder object; this is reused throughout the
         ;; rest of the function.
         (builder (LLVMCreateBuilder)))

      ;; Use the c-call calling convention (the default)
      (LLVMSetFunctionCallConv fac (cffi:foreign-enum-value 'LLVMCallConv
                                                            :LLVMCCallConv))

      ;; Create 4 new basic blocks: entry, iftrue, iffalse, end
      (let* ((entry (LLVMAppendBasicBlock fac "entry"))
             (iftrue (LLVMAppendBasicBlock fac "iftrue"))
             (iffalse (LLVMAppendBasicBlock fac "iffalse"))
             (end (LLVMAppendBasicBlock fac "end"))
             ;; get 0th function argument
             (n (LLVMGetParam fac 0))
             ;; make some extra vars to stick stuff in
             res-iftrue res-iffalse)

        ;; Create entry BB
        (LLVMPositionBuilderAtEnd builder entry)
        (let* ((IfNode (LLVMBuildICmp builder :LLVMIntEQ n
                                      (LLVMConstInt (LLVMInt32Type) 0 nil)
                                      "n == 0")))
          (LLVMBuildCondBr builder IfNode iftrue iffalse))

        ;; Create true BB
        (LLVMPositionBuilderAtEnd builder iftrue)
        (setf res-iftrue (LLVMConstInt (LLVMInt32Type) 1 nil))
        (LLVMBuildBr builder end)

        ;; Create false BB
        (LLVMPositionBuilderAtEnd builder iffalse)
        (let* ((n-minus (LLVMBuildSub builder n (LLVMConstInt (LLVMInt32Type) 1 nil) "n - 1"))
               (call-fac (LLVMBuildCall builder fac (list n-minus) "fac(n - 1)")))
          (setf res-iffalse (LLVMBuildMul builder n call-fac "n * fac(n - 1)")))
        (LLVMBuildBr builder end)

        ;; Create end BB
        (LLVMPositionBuilderAtEnd builder end)
        (let ((res (LLVMBuildPhi builder (LLVMInt32Type) "result")))
          (LLVMAddIncoming res res-iftrue iftrue)
          (LLVMAddIncoming res res-iffalse iffalse)
          (LLVMBuildRet builder res)))

      ;; Verify that module is valid
      (when (LLVMVerifyModule mod :LLVMPrintMessageAction (cffi:null-pointer))
        (error "Module didn't verify!"))

      ;; Dump textual description for debugging purposes
      (LLVMDumpValue fac)
      ;; Run some optimizations
      (LLVMRunFunctionPassManager *jit-pass-manager* fac)
      (LLVMDumpValue fac)

      ;; Clean up
      (LLVMDisposeBuilder builder)
      fac)))

;; Run the function!
(defun run-fun (fun n)
  (let ((fun-ptr (LLVMGetPointerToGlobal *jit-execution-engine* fun)))
    (cffi:foreign-funcall-pointer fun-ptr () :int64 n :int64)))


