declare {i32, i1} @llvm.sadd.with.overflow.i32(i32 %a, i32 %b)

define i32 @add(i32, i32) {
  %l = and i32 %0, 3
  %l0 = icmp ne i32 %l, 0
  br i1 %l0, label %undef, label %go1
go1:
  %r = and i32 %1, 3
  %r0 = icmp ne i32 %r, 0
  br i1 %r0, label %undef, label %go2
go2:
  %3 = lshr i32 %0, 2
  %4 = lshr i32 %1, 2
  %res = call {i32, i1} @llvm.sadd.with.overflow.i32(i32 %3, i32 %4)
  %5 = extractvalue {i32, i1} %res, 0
  %ov = extractvalue {i32, i1} %res, 1
  br i1 %ov, label %undef, label %go3
go3:
  %6 = shl i32 %5, 2
  ret i32 %6
undef:
  unreachable
}


define i32 @add3(i32, i32, i32) {
  %4 = call i32 @add(i32 %0, i32 %1)
  %5 = call i32 @add(i32 %4, i32 %2)
  ret i32 %5
}
