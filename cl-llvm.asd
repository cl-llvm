;; -*- Lisp -*-

(defsystem :cl-llvm
  :author "James Y Knight"
  :depends-on (:cffi)
  :version "0.1"
  :components
  ((:module "src"
    :serial t
    :components ((:file "packages")
                 (:file "load-c-lib")
                 (:module "generated"
                          :serial t
                          :components ((:file "core")
                                       (:file "analysis")
                                       (:file "target")
                                       (:file "execution-engine")
                                       (:file "transforms-scalar")
                                       (:file "transforms-ipo")
                                       (:file "llvm-extras")))
                 (:file "stuff")
                 (:file "packages-post")))))

;; HACK, how do you really do this? Presumably there's a way with ASDF
;; to note that a shared lib should be loaded?

(require :cffi)

(pushnew (merge-pathnames "src/" (make-pathname :name nil  :type nil :defaults *load-truename*))
         cffi:*foreign-library-directories*)

(cffi:define-foreign-library cl-llvm
    (t (:or (:default "cl-llvm") "cl-llvm.so")))

(cffi:use-foreign-library cl-llvm)
