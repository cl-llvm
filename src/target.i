%include "typemaps.i"

%ignore LLVMIntPtrType;
%rename CLLLVM_LLVMIntPtrTypeInContext "%%LLVMIntPtrTypeInContext";

%include "llvm-c/Target.h"

LLVMTypeRef CLLLVM_LLVMIntPtrTypeInContext(LLVMContextRef Context, LLVMTargetDataRef TD);
