(in-package :llvm)

;; Enable verbose assembly output from codegen
;(cffi::defcvar ("_ZN4llvm16PrintMachineCodeE" *print-machine-code*) :boolean)
;(setf *print-machine-code* t)


;; Load up the native codegen.
(CLLLVM_LLVMInitializeNativeTarget)

;; A global context. Most of LLVM is only thread-safe within a single "context".  There is an
;; internal-to-LLVM C default global context, implicitly used by a number of functions like
;; LLVM*Type (without InContext on the end), but I make a lisp-side global context here for clarity.
;;
;; Delete with LLVMContextDispose when done.
;;
;; ...FIXME...or not. For some reason making a custom context makes things fail to verify, stating
;; that I've mixed contexts. Need to figure out if this is my fault or a bug in LLVM.
;;
;; So just use the standard global context
;;  (defvar *llvm-context* (LLVMContextCreate))
(defvar *llvm-context* (LLVMGetGlobalContext))

;; Top-level LLVM module for running the JIT in. Other modules can be made for codegen-to-disk, but
;; only a single module for JIT execution can exist in the process.
(defvar *jit-module* (LLVMModuleCreateWithName "jit-module"))

;; Module provider...dunno what the purpose of this is, it wraps the module
;; Delete with LLVMDisposeModuleProvider; don't delete the wrapped module
(defvar *jit-module-provider* (LLVMCreateModuleProviderForExistingModule *jit-module*))

;; Create the JIT compiler, optimization level 2 (whatever that means).  This call fails if you run
;; it twice in a process. (which is why we can have only one module for JIT code)
(defvar *jit-execution-engine* (LLVMCreateJITCompiler *jit-module-provider* 2))

;; Optimization passes. Cleanup with LLVMDisposePassManager.
(defvar *jit-pass-manager* (LLVMCreateFunctionPassManager *jit-module-provider*))
(let ((pass *jit-pass-manager*))
  (LLVMAddTargetData (LLVMGetExecutionEngineTargetData *jit-execution-engine*) pass)
  (LLVMAddConstantPropagationPass pass)
  (LLVMAddInstructionCombiningPass pass)
  (LLVMAddPromoteMemoryToRegisterPass pass)
  (LLVMAddGVNPass pass)
  (LLVMAddCFGSimplificationPass pass))
