(in-package :llvm)

;; Export all symbols starting with "LLVM".

(labels ((starts-with (s prefix)
           (let* ((x (mismatch s prefix)))
             (or (null x) (= x (length prefix))))))
  (do-symbols (symbol (find-package :llvm))
    (let ((symbol-name (symbol-name symbol)))
      (when (or (starts-with symbol-name "LLVM") (starts-with symbol-name "CLLLVM"))
        (export symbol)))))

(export '(*llvm-context* *jit-module* *jit-module-provider* *jit-execution-engine* *jit-pass-manager*))
