%include "typemaps.i"

%rename LLVMCreateJITCompiler "%%LLVMCreateJITCompiler";

%include "llvm-c/ExecutionEngine.h"

%insert("swiglisp") %{

(defun LLVMCreateJITCompiler (provider opt)
  (cffi:with-foreign-objects ((out-engine :pointer)
                              (out-error-str :pointer))
    (if (null (%LLVMCreateJITCompiler out-engine provider opt out-error-str))
        (cffi:mem-ref out-engine :pointer)
        (let* ((error-str (cffi:mem-ref out-error-str :pointer))
               (error-str-lisp (cffi:foreign-string-to-lisp error-str)))
          (LLVMDisposeMessage error-str)
          (error "LLVMCreateJITCompiler: ~s" error-str-lisp)))))

%}

// TODO: lots of functions that need special wrappings
