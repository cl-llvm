%include "typemaps.i"

 // LLVMAttribute is a bitfield
%feature("bitfield") LLVMAttribute;

typedef unsigned char uint8_t;

// Rename/wrap functions that take pointer arguments

%rename LLVMFunctionType "%%LLVMFunctionType";
%rename LLVMGetParamTypes "%%LLVMGetParamTypes";
%rename LLVMGetStructElementTypes "%%LLVMGetStructElementTypes";
%rename LLVMConstArray "%%LLVMConstArray";
%rename LLVMConstVector "%%LLVMConstVector";
%rename LLVMConstGEP "%%LLVMConstGEP";
%rename LLVMConstInBoundsGEP "%%LLVMConstInBoundsGEP";
%rename LLVMConstExtractValue "%%LLVMConstExtractValue";
%rename LLVMConstInsertValue "%%LLVMConstInsertValue";
%rename LLVMGetParams "%%LLVMGetParams";
%rename LLVMAddIncoming "%%LLVMAddIncoming";
%rename LLVMBuildAggregateRet "%%LLVMBuildAggregateRet";
%rename LLVMBuildInvoke "%%LLVMBuildInvoke";
%rename LLVMBuildGEP "%%LLVMBuildGEP";
%rename LLVMBuildInBoundsGEP "%%LLVMBuildInBoundsGEP";
%rename LLVMBuildCall "%%LLVMBuildCall";

// Rename/wrap "InContext" functions.
%ignore LLVMModuleCreateWithName;
%rename LLVMModuleCreateWithNameInContext "%%LLVMModuleCreateWithNameInContext";
%ignore LLVMInt1Type;
%rename LLVMInt1TypeInContext "%%LLVMInt1TypeInContext";
%ignore LLVMInt8Type;
%rename LLVMInt8TypeInContext "%%LLVMInt8TypeInContext";
%ignore LLVMInt16Type;
%rename LLVMInt16TypeInContext "%%LLVMInt16TypeInContext";
%ignore LLVMInt32Type;
%rename LLVMInt32TypeInContext "%%LLVMInt32TypeInContext";
%ignore LLVMInt64Type;
%rename LLVMInt64TypeInContext "%%LLVMInt64TypeInContext";
%ignore LLVMIntType;
%rename LLVMIntTypeInContext "%%LLVMIntTypeInContext";
%ignore LLVMFloatType;
%rename LLVMFloatTypeInContext "%%LLVMFloatTypeInContext";

%ignore LLVMDoubleType;
%rename LLVMDoubleTypeInContext "%%LLVMDoubleTypeInContext";
%ignore LLVMX86FP80Type;
%rename LLVMX86FP80TypeInContext "%%LLVMX86FP80TypeInContext";
%ignore LLVMFP128Type;
%rename LLVMFP128TypeInContext "%%LLVMFP128TypeInContext";
%ignore LLVMPPCFP128Type;

%rename LLVMPPCFP128TypeInContext "%%LLVMPPCFP128TypeInContext";
%ignore LLVMStructType;
%rename LLVMStructTypeInContext "%%LLVMStructTypeInContext";
%ignore LLVMVoidType;

%rename LLVMVoidTypeInContext "%%LLVMVoidTypeInContext";
%ignore LLVMLabelType;
%rename LLVMLabelTypeInContext "%%LLVMLabelTypeInContext";
%ignore LLVMOpaqueType;
%rename LLVMOpaqueTypeInContext "%%LLVMOpaqueTypeInContext";

%ignore LLVMConstString;
%rename LLVMConstStringInContext "%%LLVMConstStringInContext";
%ignore LLVMConstStruct;
%rename LLVMConstStructInContext "%%LLVMConstStructInContext";
%ignore LLVMAppendBasicBlock;
%rename LLVMAppendBasicBlockInContext "%%LLVMAppendBasicBlockInContext";
%ignore LLVMInsertBasicBlock;
%rename LLVMInsertBasicBlockInContext "%%LLVMInsertBasicBlockInContext";
%ignore LLVMCreateBuilder;
%rename LLVMCreateBuilderInContext "%%LLVMCreateBuilderInContext";


// Discard broken functions, in favor of those defined in llvm-extras.cpp
%ignore LLVMAddFunctionAttr;
%ignore LLVMRemoveFunctionAttr;

// LLVMConstInt is replaced in llvm-extras.i with a bignum-capable fn
%rename LLVMConstInt "%%LLVMConstInt";

%insert("swiglisp") %{
;; A little hack make the Name argument for LLVMBuild* be optional and
;; default to the empty string.
(defmacro wrap-defcfun ((c-name lisp-name &rest options) return-type &body args)
  (if (and (eql (mismatch "LLVMBuild" c-name) 9)
                 (equal (last args) '((Name :string))))
      (let ((real-lisp-name (intern (concatenate 'string "%" (symbol-name lisp-name))
                                    (symbol-package lisp-name)))
            (mod-args (map 'list #'car (butlast args))))
        `(progn
           ( cffi:defcfun (,c-name ,real-lisp-name ,@options) ,return-type ,@args)
           (defmacro ,lisp-name (,@mod-args &optional (name ""))
             `(,',real-lisp-name ,,@mod-args ,name))))
      `( cffi:defcfun (,c-name ,lisp-name ,@options) ,return-type ,@args)))
%}
%include "llvm-c/Core.h"


%insert("swiglisp") %{
;;; The wrappers to expose the "InContext" versions of the functions
;;; in a more lispy way.
(declaim (special *llvm-context*))

(defun LLVMModuleCreateWithName (ModuleId &optional (context *llvm-context*))
  (%LLVMModuleCreateWithNameInContext ModuleId context))

(defun LLVMInt1Type (&optional (context *llvm-context*))
  (%LLVMInt1TypeInContext context))

(defun LLVMInt8Type (&optional (context *llvm-context*))
  (%LLVMInt8TypeInContext context))

(defun LLVMInt16Type (&optional (context *llvm-context*))
  (%LLVMInt16TypeInContext context))

(defun LLVMInt32Type (&optional (context *llvm-context*))
  (%LLVMInt32TypeInContext context))

(defun LLVMInt64Type (&optional (context *llvm-context*))
  (%LLVMInt64TypeInContext context))

(defun LLVMIntType (NumBits &optional (context *llvm-context*))
  (%LLVMIntTypeInContext context NumBits))

(defun LLVMFloatType (&optional (context *llvm-context*))
  (%LLVMFloatTypeInContext context))

(defun LLVMDoubleType (&optional (context *llvm-context*))
  (%LLVMDoubleTypeInContext context))

(defun LLVMX86FP80Type (&optional (context *llvm-context*))
  (%LLVMX86FP80TypeInContext context))

(defun LLVMFP128Type (&optional (context *llvm-context*))
  (%LLVMFP128TypeInContext context))

(defun LLVMPPCFP128Type (&optional (context *llvm-context*))
  (%LLVMPPCFP128TypeInContext context))

;; LLVMStructTypeInContext handled below.

(defun LLVMVoidType (&optional (context *llvm-context*))
  (%LLVMVoidTypeInContext context))

(defun LLVMLabelType (&optional (context *llvm-context*))
  (%LLVMLabelTypeInContext context))

(defun LLVMOpaqueType (&optional (context *llvm-context*))
  (%LLVMOpaqueTypeInContext context))

;; LLVMConstStringInContext handled below
;; LLVMConstStructInContext handled below

(defun LLVMAppendBasicBlock (Fn Name &optional (context *llvm-context*))
  (%LLVMAppendBasicBlockInContext context Fn Name))

(defun LLVMInsertBasicBlock (BB Name &optional (context *llvm-context*))
  (%LLVMInsertBasicBlockInContext context BB Name))

(defun LLVMCreateBuilder (&optional (context *llvm-context*))
  (%LLVMCreateBuilderInContext context))


;; More complex wrappers for dealing with pointers.

(defmacro with-array-and-length ((array len list) &body body)
  (let ((list-v (gensym "list")))
    `(let* ((,list-v ,list)
            (,len (length ,list-v)))
       (cffi:with-foreign-object (,array :pointer ,len)
         (loop for l in ,list-v
               for i from 0
               do
               (setf (cffi:mem-aref ,array :pointer i) l))
         (progn ,@body)))))

(defun LLVMFunctionType (ret params is-var-arg)
  "Combines the C API's ParamTypes array and ParamCount arguments into
a single list PARAMS."
  (with-array-and-length (array len params)
    (%LLVMFunctionType ret array len is-var-arg)))

(defun LLVMGetParamTypes (function-type)
  "Returns a list of param types rather than filling out a Dest pointer argument"
  (let ((len (LLVMCountParamTypes function-type)))
    (cffi:with-foreign-object (array :pointer len)
      (%LLVMGetParamTypes function-type array)
      (loop for i from 0 below len
            collect
            (cffi:mem-aref array :pointer i)))))

(defun LLVMStructType (element-types is-packed &optional (context *llvm-context*))
  (with-array-and-length (array len element-types)
    (%LLVMStructTypeInContext context array len is-packed)))

(defun LLVMGetStructElementTypes (struct-type)
  "Returns a list of param types rather than filling out a Dest pointer argument"
  (let ((len (LLVMCountStructElementTypes struct-type)))
    (cffi:with-foreign-object (array :pointer len)
      (%LLVMGetStructElementTypes struct-type array)
      (loop for i from 0 below len
            collect
            (cffi:mem-aref array :pointer i)))))

(defun LLVMConstString (str dont-null-terminate &optional (context *llvm-context*))
  (cffi:with-foreign-string ((foreign-string num-bytes) str)
    (%LLVMConstStringInContext context foreign-string num-bytes dont-null-terminate)))

(defun LLVMConstStruct (vals packed-p &optional (context *llvm-context*))
  (with-array-and-length (array len vals)
    (%LLVMConstStructInContext context array len packed-p)))

(defun LLVMConstArray (type vals)
  (with-array-and-length (array len vals)
    (%LLVMConstArray type array len)))

(defun LLVMConstVector (vals)
  (with-array-and-length (array len vals)
    (%LLVMConstVector array len)))

(defun LLVMConstGEP (ptr indices)
 (with-array-and-length (array len indices)
   (%LLVMConstGEP ptr array len)))

(defun LLVMConstInBoundsGEP (ptr indices)
 (with-array-and-length (array len indices)
   (%LLVMConstInBoundsGEP ptr array len)))

(defun LLVMConstExtractValue (AggConstant indices)
 (with-array-and-length (array len indices)
   (%LLVMConstExtractValue AggConstant array len)))

(defun LLVMConstInsertValue (AggConstant ValConstant indices)
 (with-array-and-length (array len indices)
   (%LLVMConstInsertValue AggConstant ValConstant array len)))

(defun LLVMGetParams (fn)
  "Returns a list of params rather than filling out a Dest pointer argument."
  (let ((len (LLVMCountParams fn)))
    (cffi:with-foreign-object (array :pointer len)
      (%LLVMGetParams fn array)
      (loop for i from 0 below len
            collect
            (cffi:mem-aref array :pointer i)))))

(defun LLVMAddIncoming (phi-node incoming-val incoming-block)
  "Unlike the C API (but like the C++ API...), takes only a single
incoming val and incoming block. Call multiple times if you want to
add multiple incoming values."
  (cffi:with-foreign-objects ((incoming-vals :pointer)
                              (incoming-blocks :pointer))
    (setf (cffi:mem-aref incoming-vals :pointer 0) incoming-val)
    (setf (cffi:mem-aref incoming-blocks :pointer 0) incoming-block)
    (%LLVMAddIncoming phi-node incoming-vals incoming-blocks 1)))

(defun LLVMBuildAggregateRet (builder vals)
 (with-array-and-length (array len vals)
   (%LLVMBuildAggregateRet builder array len)))

(defun LLVMBuildInvoke (builder fn args then catch &optional (name ""))
  (with-array-and-length (array len args)
    (%LLVMBuildInvoke builder fn array len then catch name)))

(defun LLVMBuildGEP (builder ptr indices &optional (name ""))
  (with-array-and-length (array len indices)
    (%LLVMBuildGEP builder ptr array len name)))

(defun LLVMBuildInBoundsGEP (builder ptr indices &optional (name ""))
  (with-array-and-length (array len indices)
    (%LLVMBuildInBoundsGEP builder ptr array len name)))

(defun LLVMBuildCall (builder fn args &optional (name ""))
  "Combines the C API's Args array and NumArgs arguments into a single
list ARGS."
  (with-array-and-length (array len args)
    (%LLVMBuildCall builder fn array len name)))

;; TODO:
;; LLVMCreateMemoryBufferWithContentsOfFile has OutMemBuf, OutMessage
;; LLVMCreateMemoryBufferWithSTDIN has OutMemBuf, OutMessage
%}
