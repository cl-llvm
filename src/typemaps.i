
// Put everything in a package
%insert("lisphead") %{
(in-package :llvm)%}

%typemap(cin) LLVMBool ":boolean";
%typemap(cout) LLVMBool ":boolean";

// These typemaps are a bit silly: all they do is provide
// documentation in the interfaces. All the actual types degenerate into
// :pointer, in any case, via defctype.

// For Core.h
%typemap(cin) LLVMAttribute "LLVMAttribute";
%typemap(cout) LLVMAttribute "LLVMAttribute";
%typemap(cin) LLVMOpcode "LLVMOpcode";
%typemap(cout) LLVMOpcode "LLVMOpcode";
%typemap(cin) LLVMTypeKind "LLVMTypeKind";
%typemap(cout) LLVMTypeKind "LLVMTypeKind";
%typemap(cin) LLVMLinkage "LLVMLinkage";
%typemap(cout) LLVMLinkage "LLVMLinkage";
%typemap(cin) LLVMVisibility "LLVMVisibility";
%typemap(cout) LLVMVisibility "LLVMVisibility";
%typemap(cin) LLVMCallConv "LLVMCallConv";
%typemap(cout) LLVMCallConv "LLVMCallConv";
%typemap(cin) LLVMIntPredicate "LLVMIntPredicate";
%typemap(cout) LLVMIntPredicate "LLVMIntPredicate";
%typemap(cin) LLVMRealPredicate "LLVMRealPredicate";
%typemap(cout) LLVMRealPredicate "LLVMRealPredicate";

%typemap(cin) LLVMContextRef "LLVMContextRef";
%typemap(cout) LLVMContextRef "LLVMContextRef";
%typemap(cin) LLVMModuleRef "LLVMModuleRef";
%typemap(cout) LLVMModuleRef "LLVMModuleRef";
%typemap(cin) LLVMTypeRef "LLVMTypeRef";
%typemap(cout) LLVMTypeRef "LLVMTypeRef";
%typemap(cin) LLVMTypeHandleRef "LLVMTypeHandleRef";
%typemap(cout) LLVMTypeHandleRef "LLVMTypeHandleRef";
%typemap(cin) LLVMValueRef "LLVMValueRef";
%typemap(cout) LLVMValueRef "LLVMValueRef";
%typemap(cin) LLVMBasicBlockRef "LLVMBasicBlockRef";
%typemap(cout) LLVMBasicBlockRef "LLVMBasicBlockRef";
%typemap(cin) LLVMBuilderRef "LLVMBuilderRef";
%typemap(cout) LLVMBuilderRef "LLVMBuilderRef";
%typemap(cin) LLVMModuleProviderRef "LLVMModuleProviderRef";
%typemap(cout) LLVMModuleProviderRef "LLVMModuleProviderRef";
%typemap(cin) LLVMMemoryBufferRef "LLVMMemoryBufferRef";
%typemap(cout) LLVMMemoryBufferRef "LLVMMemoryBufferRef";
%typemap(cin) LLVMPassManagerRef "LLVMPassManagerRef";
%typemap(cout) LLVMPassManagerRef "LLVMPassManagerRef";
%typemap(cin) LLVMUseIteratorRef "LLVMUseIteratorRef";
%typemap(cout) LLVMUseIteratorRef "LLVMUseIteratorRef";

// For ExecutionEngine.h
%typemap(cin) LLVMGenericValueRef "LLVMGenericValueRef";
%typemap(cout) LLVMGenericValueRef "LLVMGenericValueRef";
%typemap(cin) LLVMExecutionEngineRef "LLVMExecutionEngineRef";
%typemap(cout) LLVMExecutionEngineRef "LLVMExecutionEngineRef";

// For Target.h
%typemap(cin) LLVMTargetDataRef "LLVMTargetDataRef";
%typemap(cout) LLVMTargetDataRef "LLVMTargetDataRef";
%typemap(cin) LLVMStructLayoutRef "LLVMStructLayoutRef";
%typemap(cout) LLVMStructLayoutRef "LLVMStructLayoutRef";
