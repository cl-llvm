#include <llvm-c/Target.h>
#include <llvm/Target/TargetData.h>
#include <llvm/ModuleProvider.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Assembly/Parser.h>
#include <llvm/PassManager.h>
#include <llvm/CodeGen/Passes.h>

#ifndef SWIG
using namespace llvm;
#endif

extern "C" {
// Declare here so the inline definition gets into the lib. Why is
// there an inline function in a binding header anyways. :(
int CLLLVM_LLVMInitializeNativeTarget() {
    LLVMInitializeNativeTarget();
}

// Functions missing in the LLVM C API



LLVMModuleRef CLLLVM_LLVMModuleProviderGetModule(LLVMModuleProviderRef modprovider) {
    return wrap(unwrap(modprovider)->getModule());
}

LLVMModuleRef CLLLVM_LLVMParseAssemblyString(const char *AsmString,
                                              LLVMModuleRef M,
                                              LLVMContextRef Context) {
    class SMDiagnostic Error;
    LLVMModuleRef res =
        wrap(ParseAssemblyString(AsmString, unwrap(M), Error, *unwrap(Context)));
    Error.Print("sbcl", errs());
}

LLVMTypeRef CLLLVM_LLVMIntPtrTypeInContext(LLVMContextRef Context, LLVMTargetDataRef TD) {
    return wrap(unwrap(TD)->getIntPtrType(*unwrap(Context)));
}

char *CLLLVM_LLVMDumpModuleToString(LLVMModuleRef module)
{
    std::string s;
    raw_string_ostream buf(s);
    unwrap(module)->print(buf, NULL);
    return strdup(buf.str().c_str());
}

char *CLLLVM_LLVMDumpTypeToString(LLVMTypeRef type)
{
    std::string s;
    raw_string_ostream buf(s);
    unwrap(type)->print(buf);
    return strdup(buf.str().c_str());
}

char *CLLLVM_LLVMDumpValueToString(LLVMValueRef value)
{
    std::string s;
    raw_string_ostream buf(s);
    unwrap(value)->print(buf);
    return strdup(buf.str().c_str());
}

// These are buggy in LLVM: they affect the return value attributes,
// not the fn attributes.
void CLLLVM_LLVMAddFunctionAttr(LLVMValueRef Fn, LLVMAttribute PA) {
  Function *Func = unwrap<Function>(Fn);
  const AttrListPtr PAL = Func->getAttributes();
  const AttrListPtr PALnew = PAL.addAttr(~0, PA);
  Func->setAttributes(PALnew);
}

void CLLLVM_LLVMRemoveFunctionAttr(LLVMValueRef Fn, LLVMAttribute PA) {
  Function *Func = unwrap<Function>(Fn);
  const AttrListPtr PAL = Func->getAttributes();
  const AttrListPtr PALnew = PAL.removeAttr(~0, PA);
  Func->setAttributes(PALnew);
}

void CLLLVM_LLVMAddRetAttr(LLVMValueRef Fn, LLVMAttribute PA) {
  Function *Func = unwrap<Function>(Fn);
  const AttrListPtr PAL = Func->getAttributes();
  const AttrListPtr PALnew = PAL.addAttr(0, PA);
  Func->setAttributes(PALnew);
}

void CLLLVM_LLVMRemoveRetAttr(LLVMValueRef Fn, LLVMAttribute PA) {
  Function *Func = unwrap<Function>(Fn);
  const AttrListPtr PAL = Func->getAttributes();
  const AttrListPtr PALnew = PAL.removeAttr(0, PA);
  Func->setAttributes(PALnew);
}

LLVMAttribute CLLLVM_LLVMGetRetAttr(LLVMValueRef Fn) {
  Function *Func = unwrap<Function>(Fn);
  const AttrListPtr PAL = Func->getAttributes();
  Attributes attr = PAL.getRetAttributes();
  return (LLVMAttribute)attr;
}

LLVMValueRef CLLLVM_LLVMConstIntOfBigVal(LLVMTypeRef IntTy,
                                         unsigned numWords,
                                         const uint64_t bigVal[]) {
    IntegerType *Ty = unwrap<IntegerType>(IntTy);
    return wrap(ConstantInt::get(Ty->getContext(),
                                 APInt(Ty->getBitWidth(), numWords, bigVal)));
}

void CLLLVM_AddPrintAsmPass(LLVMPassManagerRef PM, char *Banner) {
    unwrap(PM)->add(createMachineFunctionPrinterPass(errs(), Banner));
}

}
