;;; This file was automatically generated by SWIG (http://www.swig.org).
;;; Version 1.3.40
;;;
;;; Do not make changes to this file unless you know what you are doing--modify
;;; the SWIG interface file instead.

(in-package :llvm)


(cffi:defcfun ("LLVMAddAggressiveDCEPass" LLVMAddAggressiveDCEPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddCFGSimplificationPass" LLVMAddCFGSimplificationPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddDeadStoreEliminationPass" LLVMAddDeadStoreEliminationPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddGVNPass" LLVMAddGVNPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddIndVarSimplifyPass" LLVMAddIndVarSimplifyPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddInstructionCombiningPass" LLVMAddInstructionCombiningPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddJumpThreadingPass" LLVMAddJumpThreadingPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddLICMPass" LLVMAddLICMPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddLoopDeletionPass" LLVMAddLoopDeletionPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddLoopIndexSplitPass" LLVMAddLoopIndexSplitPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddLoopRotatePass" LLVMAddLoopRotatePass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddLoopUnrollPass" LLVMAddLoopUnrollPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddLoopUnswitchPass" LLVMAddLoopUnswitchPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddMemCpyOptPass" LLVMAddMemCpyOptPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddPromoteMemoryToRegisterPass" LLVMAddPromoteMemoryToRegisterPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddReassociatePass" LLVMAddReassociatePass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddSCCPPass" LLVMAddSCCPPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddScalarReplAggregatesPass" LLVMAddScalarReplAggregatesPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddSimplifyLibCallsPass" LLVMAddSimplifyLibCallsPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddTailCallEliminationPass" LLVMAddTailCallEliminationPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddConstantPropagationPass" LLVMAddConstantPropagationPass) :void
  (PM LLVMPassManagerRef))

(cffi:defcfun ("LLVMAddDemoteMemoryToRegisterPass" LLVMAddDemoteMemoryToRegisterPass) :void
  (PM LLVMPassManagerRef))


