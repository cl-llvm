%include "typemaps.i"

%rename CLLLVM_LLVMAddFunctionAttr "LLVMAddFunctionAttr";
%rename CLLLVM_LLVMRemoveFunctionAttr "LLVMRemoveFunctionAttr";
%rename CLLLVM_LLVMAddRetAttr "LLVMAddRetAttr";
%rename CLLLVM_LLVMRemoveRetAttr "LLVMRemoveRetAttr";
%rename CLLLVM_LLVMGetRetAttr "LLVMGetRetAttr";

%rename CLLLVM_LLVMConstIntOfBigVal "LLVMConstIntOfBigVal";

%include "./llvm-extras.cpp"

%insert("swiglisp") %{
(defun LLVMConstInt (Ty Num)
  (declare (type integer Num))
  ;; If the type is <= 64 bits, it's easy: we can just pass the
  ;; (truncated) number into %LLVMConstInt directly.  Otherwise,
  ;; convert the number to a bunch of 64-bit words, and pass those to
  ;; LLVMConstIntOfBigVal.
  (let ((bitwidth (LLVMGetIntTypeWidth Ty)))
    (if (< bitwidth 65)
        (%LLVMConstInt Ty (logand Num #xffffffffffffffff) 0)
        (let ((n-words (ceiling bitwidth 64)))
          (cffi:with-foreign-object (array :unsigned-long-long n-words)
            (loop for i from 0 below n-words
                  do
                  (setf (cffi:mem-aref array :unsigned-long-long i)
                        (logand Num #xffffffffffffffff))
                  (setf Num (ash Num -64)))
            (LLVMConstIntOfBigVal Ty n-words array))))))
%}
